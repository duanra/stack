
                  /*RESOLUTION DE A*Z=B*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <conio.h>

int main()
{
         int i,j,l;/*c=0,d=2,e=0,f=1*/
       float a[81][81],b[81],x[11],y[11],w[11][11],z[81],h=0.2,k=0.1,p=2*(pow((h/k),2)+1),q=pow((h/k),2),g,s;
       int /*nombre de subdivision*/ m=10,n=10; /*m=(d-c)/h et n=(f-e)/k*/

/*--------------------------------ELEMENTS DE LA MATRICE AU PREMIER MEMBRE A[i][j]--------------------------------*/

 for(i=0;i<=80;i++){
                    for(j=0;j<=80;j++){a[i][j]=0;//INITIALISATION A ZERO DES ELEMENTS DE LA MATRICE
                                      }
                   }
     for(l=0;l<=80;l++){a[l][l]=p;// ELEMENTS DE LA DIAGONALE PRINCIPALE
                       }
         for(i=0;i<=79;i++){a[i][i+1]=-1;//ELEMENTS DE LA DIAGONALE, AU DESSUS DE LA DIAGONALE PRINCIPALE
                            a[i+1][i]=-1;//ELEMENTS DE LA DIAGONALE, EN DESSOUS DE LA DIAGONALE PRINCIPALE
                           }
              for(i=8;i<=79;i+=9){a[i][i+1]=0;//ZERO SUR LA DIAGONALE, AU DESSUS DE LA DIAGONALE PRINCIPALE
                                  a[i+1][i]=0;//ZERO SUR LA DIAGONALE, EN DESSOUS DE LA DIAGONALE PRINCIPALE
                                 }
                   for(j=9;j<=80;j++){a[j-9][j]=-q; //ELEMENTS DE LA DIAGONALE:BETA SUPERIEUR
                                      a[j][j-9]=-q;//ELEMENTS DE LA DIAGONALE:BETA INFERIEUR
                                     }

 /*--------------------------------ELEMENTS DE LA MATRICE AU SECOND MEMBRE B[i]-----------------------------------*/
    /*ELEMENTS DE LA MATRICE W[i][j]*/
 for(i=0;i<=10;i++){
                    for(j=0;j<=10;j++){w[i][j]=0;
                                      }
                    x[i]=i*h;y[i]=i*k;/*VALEURS DE x ET y A CHAQUE POINTS DE LA SUBDIVISION */
                   }
       for(l=0;l<=10;l++){ /*SOLUTIONS AUX LIMITES:*/
                           w[0][l]=1;
                           w[l][0]=1;
                           w[10][l]=exp(2*y[l]);
                           w[l][10]=exp(x[l]);
                         }

    /*ELEMENTS DE LA MATRICE B[i]*/

 for(i=9;i>=1;i--){
                   for(j=1;j<=9;j++){
                                     b[(j+(n-1)*(m-1-i))-1]=-pow(h,2)*exp(x[j]*y[i])*(pow(x[j],2)+pow(y[i],2));
                                    }   //(u+(n-1)*(m-1-t))-1:changement d'indice
                  }
      for(i=0;i<=8;i++){b[i]+=q*w[i+1][10];
                       }
           for(j=72;j<=80;j++){b[j]+=q*w[j-71][0];
                              }
                for(l=0,i=9;l<=80,i>=1;l+=9,i--){b[l]+=w[0][i];
                                                }
                    for(i=8,j=9;i<=80,j>=1;i+=9,j--){b[i]+=w[10][j];
                                                    }

/*----------------------------RESOLUTION DE A[i][j]*Z[i]=B[i] PAR LA METHODE DE GAUSS----------------------------*/

    /*TRIANGULARISATION DE LA MATRICE*/

 for(l=0;l<=79;l++){
                    for(i=l+1;i<=80;i++){g=a[i][l]/a[l][l];
                                         b[i]-=g*b[l];
                                         for(j=l;j<=80;j++){a[i][j]-=g*a[l][j];
                                                           }
                                        }
	               }

       /*RESOLUTION DU SYSTEME TRIANGULAIRE*/

 z[80]=b[80]/a[80][80];
  for(i=79;i>=0;i--){
                     for(j=i+1,s=0;j<=80;j++){s+=a[i][j]*z[j];
                                             }
                     z[i]=(b[i]-s)/a[i][i];
                    }

      /*AFFICHAGE DES RESULTATS*/

    printf("\n  VALEURS DE u(x,y) POUR CHAQUE\n\n");
    printf("\t    NOEUD:\n\n");
    for(i=1;i<=9;i++){

                      for(j=1;j<=9;j++){
                                        printf("  [%d,%d] => u(%.1f;%.1f)=%f\n",j,i,x[j],y[i],z[(j+(9*(9-i)))-1]);
                                       }
                     printf("\n  ****************************\n\n");
                      }

  getch();
  system("pause");
  return 0;
  }
