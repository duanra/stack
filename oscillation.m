%amortissement faible
[x,y]=meshgrid(0:0.1:10,0:0.1:10);
A = input('Entrer l amplitude du système A = ');
a = input('Entrer la constante d amortissement a = ');
w = input('Entrer la valeur de la pulsation w = ');
phi = input('Entrer la phase de l oscillation phi = ');
z=A*exp(-a*x/2).*sin(w*x-phi);
figure(A);
surf(x,y,z);
title('amortissement faible d un oscillateur à une dimension');
