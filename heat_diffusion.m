%Affiche la courbe de diffusion de la chaleur
%entre les positions ]0,1[ � chaque instant 0 < t <1
pi = 3.141593; h=0.1; k=0.04;
alpha = 1 / pi; lambda = alpha^2 * k / h^2;
a=zeros(9,9); b=zeros(1,9);
for i=1:9
    a(i,i)=1+2*lambda;
end
for i=2:9
    a(i-1,i)=-lambda;
    a(i,i-1)=-lambda;
end
for i=1:9
    b(i)=cos(pi*(i*h-0.5));
end
w=b';
for i= 1:25
   w=horzcat(w,inv(a)^(i)*b');
end
w
figure(1);
grid on; hold on;
xlabel('Positions x');
ylabel('Approximations w(xi,tj)');
for i= 1:26 
    x = 0.1:0.1:0.9;
    y=w(1:9,i);
    plot(x,y,'color',[i/26 0  .6], 'linewidth', 1);
end
