%Résolution d'une équation différentielle par la méthode
    %d'approximation en différences finies
% -u" + pi^2 = f dans ]0,1[
% u(0) = u(1) = 0
a=0; b=1; N=20; ua = 0 ; ub = 0;
h=(b-a)/(N+1);
x=[a+h:h:b-h];
f=2*pi*pi*x.*sin(pi*x)-2*pi*cos(pi*x);
e1=ones(N,1);
e2=[-e1 2*e1 -e1];
R=spdiags(e2,-1:1,N,N); full(R);
I=speye(N,N);
M=pi*pi*I;
A=(R/(h^2))+ M; full(A);
B=f';
B(1) = B(1) + ua/(h*h);
B(N) = B(N) + ub/(h*h);
uh = A\B;
%redimensionnement
x=[a x b];
us=[ua ; uh; ub];
plot(x,us); xlabel('x'); ylabel('y');

